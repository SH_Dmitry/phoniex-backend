//express

import express = require("express");
import cors = require("cors");
import bodyParser = require("body-parser");
import {setStrategy} from "./src/security/auth.service";

const app = express();
const PORT = process.env.PORT || 3001;
//routes
const mainRouter = require("./src/routes/main.router.js");

//Passport
const passport = require('passport');
setStrategy();
app.use(passport.initialize(undefined));

//Query
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}));
app.use(bodyParser.raw());
app.use(cors());
app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});


app.use("/", mainRouter);

app.use(function (req, res, next) {
    res.status(404).send("Not Found")
});

app.listen(PORT, () => {
    console.log(`App listening to ${PORT}....`);
    console.log('Press Ctrl+C to quit.');
});
