import express = require("express");
import {materialRouter} from "../domains/material/routes/material.router";
import {partsRouter} from "../domains/part/parts.router";
import {reportRouter} from "../domains/reports/reports.router";
import {typeRouter} from "../domains/type/types.router"
import {userRouter} from "../domains/user/user.router";
import {importRouter} from "../domains/services/import/import.routes";

const mainRouter = express.Router();

mainRouter.use("/user", userRouter);
mainRouter.use("/material", materialRouter);
mainRouter.use("/part", partsRouter);
mainRouter.use("/report",reportRouter);
mainRouter.use("/type", typeRouter);
mainRouter.use("/import", importRouter);

module.exports = mainRouter;