import {Response} from "express";

const stream = require('stream');

export const SendResponse = (res: Response, data: Promise<any>, hasResult: boolean = true) => {
    data.then(
        response => {
            if (hasResult) {
                if (response) {
                    res.status(200).json(response);
                } else {
                    res.status(500).send("No data returned");
                }
            } else {
                res.status(201).end();
            }
        }
    ).catch(error => {
        console.log(error);
        res.status(500).send(error);
    })
};

export const SendFile = (res: Response, data: Buffer, filename: string) => {
    try {
        const fileName = filename + '.doc';
        const readStream = new stream.PassThrough();
        readStream.end(data);
        res.set('Content-disposition', 'attachment; filename=' + fileName);
        res.set('Content-Type', 'text/plain');
        readStream.pipe(res);
    } catch (error) {
        res.status(500)
            .send(error);
    }
};