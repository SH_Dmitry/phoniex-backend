import sql = require("mssql");
import {Request} from "mssql";

const config = {
    user:  "astpp_connector",
    password: "y55t",
    server: "localhost",
    database: "astpp_db",
    encrypted: false
};

export const getConnection = async (): Promise<Request> => {
    let pool = new sql.ConnectionPool(config);
    let poolConnect = pool.connect();
    await poolConnect;
    return pool.request();
};