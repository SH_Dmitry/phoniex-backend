import {IDocTableTemplate} from "./doc.table.types";
import {IGroup, IUndergroup} from "../../domains/material/types";
import {IPartMaterialItem} from "../../domains/part/part.materials/types";

export function createHeaderTemplate(name: string, decimal_num: string) {
    const template: IDocTableTemplate = {
        columnWidths: [7, 7, 7, 7, 10, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6],
        rowList: [
            {
                height: 200,
                cells: [
                    {
                        value: "Дубл.",
                    },
                    {}, {}, {},
                    {
                        hasBoarder: true,
                        mergeCol: 6
                    },
                    {}, {}, {}, {}, {}
                ]
            },
            {
                height: 200,
                cells: [
                    {
                        value: "Взам.",
                    }, {}, {}, {},
                    {
                        hasBoarder: true,
                    },
                    {}, {}, {}, {}, {}, {}, {}, {}, {}, {},
                ]
            },
            {
                height: 200,
                cells: [
                    {
                        value: "Подл.",
                    },
                    {}, {}, {},
                    {
                        hasBoarder: true,
                    },
                    {}, {}, {}, {}, {}, {}, {}, {}, {}, {}
                ]
            },
            {
                height: 200,
                cells: [
                    {mergeCol: 15},
                ]
            },
            {
                height: 200,
                cells: [
                    {},
                    {
                        value: "Наименование",
                        mergeCol: 2
                    },
                    {
                        value: "Обозначение",
                        mergeCol: 2
                    },
                    {
                        value: "Ведомость норм расхода материалов",
                        mergeCol: 6,

                    },
                    {
                        value: "Номер альбома",
                        mergeCol: 2
                    },
                    {
                        value: "Листов",
                    },
                    {}
                ]
            },
            {
                height: 600,
                cells: [
                    {
                        value: "Изделие",
                    },
                    {
                        value: name,
                        mergeCol: 2
                    },
                    {
                        value: decimal_num,
                        mergeCol: 2
                    },
                    {
                        mergeCol: 6
                    },
                    {
                        mergeCol: 2
                    }, {}, {}
                ]
            },
        ]
    };
    return template
}


export function createMaterialRowsTemplate(materials: Array<IPartMaterialItem>, group: Array<IGroup>, undergroup: Array<IUndergroup>) {
    let template: IDocTableTemplate = {
        columnWidths: [30, 30, 10, 10, 20],
        rowList: [
            //Header
            {
                height: 200,
                cells: [
                    {
                        value: "Наименование материала",
                    },
                    {
                        value: "Сортамент",
                    },
                    {
                        value: "Количество",
                    },
                    {
                        value: "Ед.изм.",
                    },
                    {
                        value: "Примечание",
                    }
                ]
            }
        ]
    };
    for (let g of group) {
        let isAdded = false;
        for(let ug of undergroup){
            if(ug.group_id===g.group_id){
                let rowCount = 0;
                for(let m of materials)
                {
                    if(m.undergroup_id ===ug.undergroup_id)
                    {
                        if(!isAdded)
                        {
                            template.rowList.push({
                                cells: [
                                    {
                                        value: g.name,
                                        mergeCol: 4
                                    }
                                ],
                                height: 200
                            });
                            isAdded= true
                        }
                        rowCount++;
                        template.rowList.push({
                            height: 200,
                            cells: [
                                {
                                    value: ug.name,
                                },
                                {
                                    value: m.forma?(m.forma + " " + m.sortament + '\n' +  m.material): m.material,
                                },
                                {
                                    value: m.count.toString(),
                                },
                                {
                                    value: m.unit_id,
                                },
                                {
                                    value: m.comment,
                                }
                            ]
                        })
                    }
                }
            }
        }
    }
    return template
}