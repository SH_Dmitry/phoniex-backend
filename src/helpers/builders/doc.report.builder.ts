import {Document, Header, Packer, PageOrientation, Paragraph} from "docx";
import {createHeaderTemplate, createMaterialRowsTemplate} from "./report.material.creator";
import {DocTableBuilder} from "./doc.table.builder";
import {getMaterialsByPartList, orderingMaterialList} from "../../domains/part/part.materials/part.materials.service";
import {getPartCountingList} from "../../domains/part/part.hierarchy/part.hierarchy.service";
import {MaterialGroupRepository} from "../../domains/material/repository/group.repository";
import {MaterialUndergroupRepository} from "../../domains/material/repository/undergroup.repository";

const groupRepository = new MaterialGroupRepository();
const undergroupRepository = new MaterialUndergroupRepository();

export class DocReportBuilder {
    constructor(
        public part_id: string,
    ) {}

    public async createReport(name:string, decimal_num:string): Promise<Buffer>{
        const tableGenerator = new DocTableBuilder();
        const parts = await getPartCountingList(this.part_id);
        const rowMaterials = await getMaterialsByPartList(parts);
        const materials = await orderingMaterialList(rowMaterials);
        const groups = await groupRepository.get();
        const undergroups = await undergroupRepository.getAll();
        const header = tableGenerator.createTableByTemplate(createHeaderTemplate(name, decimal_num));
       const table = tableGenerator.createTableByTemplate(createMaterialRowsTemplate(materials,groups,undergroups ));
        const doc = new Document();
        doc.addSection({
            properties: {
                orientation: PageOrientation.LANDSCAPE
            },
            headers: {
                default: new Header({
                    children: [header],
                }),
            },
            children: [new Paragraph(""),table],
        });
        return await Packer.toBuffer(doc)
    }
}