import {BorderStyle, HeightRule, Paragraph, Table, TableCell, TableRow, WidthType} from "docx";

import {IDocTableCell, IDocTableRow, IDocTableTemplate} from "./doc.table.types";


export class DocTableBuilder {
    public createTableByTemplate(template: IDocTableTemplate): Table {
        return new Table({
            rows: this.createTableRow(template.rowList),
            columnWidths: template.columnWidths,
            width: {
                size: 100,
                type: WidthType.PERCENTAGE
            }
        });
    }

    private createTableRow(rows: Array<IDocTableRow>): Array<TableRow> {
        let tableRows = [] as Array<TableRow>;
        for (let row of rows) {
            let cells = [] as Array<TableCell>;
            for (let cell of row.cells) {
                cells = cells.concat(this.createTableCell(cell))
            }
            tableRows.push(new TableRow({
                height: {
                    height: row.height,
                    rule: HeightRule.EXACT
                },
                children: cells
            }))
        }
        return tableRows
    };

    private createTableCell(cell: IDocTableCell) {
        let cells = [] as Array<TableCell>;
        const value = (cell.value) ? new Paragraph(cell.value) : new Paragraph("");
        const border = (cell.hasBoarder) ?
            {
                style: BorderStyle.NONE,
                size: 1,
                color: "black",
            } : {
                style: BorderStyle.SINGLE,
                size: 1,
                color: "black",
            };
        const mergeCol = (cell.mergeCol) ? cell.mergeCol : 1;
        const mergeRow = (cell.mergeRow) ? cell.mergeRow : 1;
        cells.push(new TableCell({
            children: [value],
            borders: {
                top: border,
                left: border,
                right: border,
                bottom: border
            },
            columnSpan: mergeCol,
            rowSpan: mergeRow
        }));

        return cells
    };
}