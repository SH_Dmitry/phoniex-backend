export type IDocTableCell = {
    value?: string,
    mergeCol?: number,
    mergeRow?: number,
    hasBoarder?: boolean,
}

export type IDocTableRow = {
    height: number,
    cells: Array<IDocTableCell>
}

export type IDocTableTemplate = {
    columnWidths: Array<number>
    rowList: Array<IDocTableRow>
}
