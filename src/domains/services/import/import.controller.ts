import {MaterialGroupRepository} from "../../material/repository/group.repository";
import {SendResponse} from "../../../middleware/response.handler";
import {IGroup, IMaterial, IUndergroup} from "../../material/types";
import {MaterialUndergroupRepository} from "../../material/repository/undergroup.repository";
import {MaterialRepository} from "../../material/repository/material.repository";
import {PartsRepository} from "../../part/parts.repository";
import {IPartSimple} from "../../part/types";
import {IUser} from "../../../../../frontend/src/modules/SecurityGateModule";
import {PartMaterialsRepository} from "../../part/part.materials/part.materials.repository";
import {IPartMaterialItem, IPartMaterials, IPartMaterialsSimple,} from "../../part/part.materials/types";
import {PartStructureRepository} from "../../part/part.structure/part.structure.repository";
import {IPartStructureItem, IPartStructures, IPartStructuresSimple} from "../../part/part.structure/types";

type IImportMessage<T> = {
    id: number,
    item_type: string, //imported item type
    status: string, // "added", "intersection", "failure"
    item: T, //imported item
    intersection?: T    // Item from db, if import has intersection
    error_message?: string //error message, if import failure
}

const groupRepository = new MaterialGroupRepository();
const undergroupRepository = new MaterialUndergroupRepository();
const materialRepository = new MaterialRepository();
const partRepository = new PartsRepository();
const partMaterialRepository = new PartMaterialsRepository();
const partStructureRepository = new PartStructureRepository();

const getGroupsIntersection = async (groups: Array<IGroup>) => {
    const messages: Array<IImportMessage<IGroup>> = new Array<IImportMessage<IGroup>>();
    let id = 0;
    if (groups && groups.length && groups[0].group_id) {
        for (const group of groups) {
            const response: IGroup = await groupRepository.getById(group.group_id);
            if (!response) {
                try {
                    await groupRepository.add(group);
                    const newMessage: IImportMessage<IGroup> = {
                        id,
                        item_type: "group",
                        status: "added",
                        item: group,
                        intersection: response
                    };
                    messages.push(newMessage);
                } catch (error) {
                    const newMessage: IImportMessage<IGroup> = {
                        id,
                        item_type: "group",
                        status: "failure",
                        item: group,
                        error_message: error.toString()
                    };
                    messages.push(newMessage);
                }
            } else {
                const newMessage: IImportMessage<IGroup> = {
                    id,
                    item_type: "group",
                    status: "intersection",
                    item: group,
                    intersection: response
                };
                messages.push(newMessage);
            }
            id = id + 1;
        }
    }
    return messages
};

const getUndergroupsIntersection = async (undergroups: Array<IUndergroup>) => {
    const messages: Array<IImportMessage<IUndergroup>> = new Array<IImportMessage<IUndergroup>>();
    let id = 0;
    if (undergroups && undergroups.length && undergroups[0].undergroup_id) {
        for (const undergroup of undergroups) {
            const response: IUndergroup = await undergroupRepository.getById(undergroup.undergroup_id);
            if (!response) {
                try {
                    await undergroupRepository.add(undergroup);
                    const newMessage: IImportMessage<IUndergroup> = {
                        id,
                        item_type: "undergroup",
                        status: "added",
                        item: undergroup,
                        intersection: response
                    };
                    messages.push(newMessage);
                } catch (error) {
                    const newMessage: IImportMessage<IUndergroup> = {
                        id,
                        item_type: "undergroup",
                        status: "failure",
                        item: undergroup,
                        error_message: error.toString()
                    };
                    messages.push(newMessage);
                }
            } else {
                const newMessage: IImportMessage<IUndergroup> = {
                    id,
                    item_type: "undergroup",
                    status: "intersection",
                    item: undergroup,
                    intersection: response
                };
                messages.push(newMessage);
            }
            id = id + 1;
        }
    }
    return messages
};

const getMaterialsIntersection = async (materials: Array<IMaterial>) => {
    const messages: Array<IImportMessage<IMaterial>> = new Array<IImportMessage<IMaterial>>();
    let id = 0;
    if (materials && materials.length && materials[0].material_id) {
        for (const material of materials) {
            const response: IMaterial = await materialRepository.getById(material.material_id);
            if (!response) {
                try {
                    await materialRepository.add(material);
                    const newMessage: IImportMessage<IMaterial> = {
                        id,
                        item_type: "material",
                        status: "added",
                        item: material,
                        intersection: response
                    };
                    messages.push(newMessage);
                } catch (error) {
                    const newMessage: IImportMessage<IMaterial> = {
                        id,
                        item_type: "material",
                        status: "failure",
                        item: material,
                        error_message: error.toString()
                    };
                    messages.push(newMessage);
                }
            } else {
                const newMessage: IImportMessage<IMaterial> = {
                    id,
                    item_type: "material",
                    status: "intersection",
                    item: material,
                    intersection: response
                };
                messages.push(newMessage);
            }
            id = id + 1;
        }
    }
    return messages
};

const getPartsIntersection = async (parts: Array<IPartSimple>, user: IUser) => {
    const messages: Array<IImportMessage<IPartSimple>> = new Array<IImportMessage<IPartSimple>>();
    if (parts && parts[0] && parts[0].decimal_num && parts[0].name) {
        let index = await partRepository.getLastIndex();
        let id = index.id;
        for (const part of parts) {
            id = id + 1;
            const response: IPartSimple = await partRepository.getByDecimalNumber(part.decimal_num);
            if (!response) {
                try {
                    part.part_id = id;
                    await partRepository.add({user_id: user.user_id, part});
                    const newMessage: IImportMessage<IPartSimple> = {
                        id,
                        item_type: "part",
                        status: "added",
                        item: part,
                        intersection: response
                    };
                    messages.push(newMessage);
                } catch (error) {
                    const newMessage: IImportMessage<IPartSimple> = {
                        id,
                        item_type: "part",
                        status: "failure",
                        item: part,
                        error_message: error.toString()
                    };
                    messages.push(newMessage);
                }
            } else {
                const newMessage: IImportMessage<IPartSimple> = {
                    id,
                    item_type: "part",
                    status: "intersection",
                    item: part,
                    intersection: response
                };
                messages.push(newMessage);
            }
        }
    }
    return messages
};

const getPartMaterialsIntersection = async (importPartMaterialsSimpleList: Array<IPartMaterialsSimple>, user: IUser) => {
    const messages: Array<IImportMessage<any>> = new Array<IImportMessage<any>>();
    let id = 0;
    if (importPartMaterialsSimpleList && importPartMaterialsSimpleList[0] && importPartMaterialsSimpleList[0].decimal_num && importPartMaterialsSimpleList[0].materials) {
        for (const importPartMaterialsSimple of importPartMaterialsSimpleList) {
            //Get part by decimal
            if (importPartMaterialsSimple.materials.length > 0) {
                const parentPart = await partRepository.getByDecimalNumber(importPartMaterialsSimple.decimal_num);
                //Get materials for this part
                const dbPartMaterials: Array<IPartMaterialItem> = await partMaterialRepository.get(parentPart.part_id);
                try {
                    if (!dbPartMaterials.length) {
                        //Add branch

                        let importResult = {
                            part_id: parentPart.part_id,
                            name: parentPart.name,
                            decimal_num: parentPart.decimal_num,
                            materials: []
                        };

                        for (const material of importPartMaterialsSimple.materials) {
                            const partWithMaterial = {
                                user_id: user.user_id,
                                part_id: parentPart.part_id,
                                ...material
                            };
                            importResult.materials.push(await partMaterialRepository.addWithParams(partWithMaterial))
                        }

                        const newMessage: IImportMessage<IPartMaterials> = {
                            id,
                            item_type: "part_materials",
                            status: "added",
                            item: importResult,
                            intersection: null
                        };
                        messages.push(newMessage);
                    } else {
                        let dbResult = {
                            part_id: parentPart.part_id,
                            name: parentPart.name,
                            decimal_num: parentPart.decimal_num,
                            materials: dbPartMaterials
                        };
                        let importResult = {
                            part_id: parentPart.part_id,
                            name: parentPart.name,
                            decimal_num: parentPart.decimal_num,
                            materials: []
                        };
                        for (const materialItem of importPartMaterialsSimple.materials) {
                            const dbMaterial = await materialRepository.getById(materialItem.material_id);
                            const importedMaterialItem: IPartMaterialItem = {
                                material_id: materialItem.material_id,
                                material: dbMaterial.material,
                                forma: dbMaterial.forma,
                                sortament: dbMaterial.sortament,
                                undergroup_id: dbMaterial.undergroup_id,
                                comment: materialItem.comment,
                                kd_count: materialItem.kd_count,
                                count: materialItem.count,
                                sizes: materialItem.sizes,
                                unit_id: materialItem.unit_id || "2",
                            };
                            importResult.materials.push(importedMaterialItem)
                        }
                        if (!materialsEqual(importResult, dbResult)) {
                            const newMessage: IImportMessage<IPartMaterials> = {
                                id,
                                item_type: "part_materials",
                                status: "intersection",
                                item: importResult,
                                intersection: dbResult
                            };
                            messages.push(newMessage);
                        }
                    }
                } catch (error) {
                    //Error branch
                    const newMessage: IImportMessage<IPartMaterialsSimple> = {
                        id,
                        item_type: "part_materials",
                        status: "failure",
                        item: importPartMaterialsSimple,
                        error_message: error.toString()
                    };
                    messages.push(newMessage);
                }
                id = id + 1;
            }

        }
    }
    return messages
};

const getPartStructuresIntersection = async (importPartStructuresList: Array<IPartStructuresSimple>, user: IUser) => {
    const messages: Array<IImportMessage<any>> = new Array<IImportMessage<any>>();
    let id = 0;
    if (importPartStructuresList && importPartStructuresList[0] && importPartStructuresList[0].decimal_num && importPartStructuresList[0].structures) {
        for (const importPartStructure of importPartStructuresList) {
            //Get part by decimal
            if (importPartStructure.structures.length > 0) {
                const parentPart = await partRepository.getByDecimalNumber(importPartStructure.decimal_num);
                const dbPartStructure: Array<IPartStructureItem> = await partStructureRepository.get(parentPart.part_id);
                if (!dbPartStructure.length) {
                    try {
                        let importResult = {
                            part_id: parentPart.part_id,
                            name: parentPart.name,
                            decimal_num: parentPart.decimal_num,
                            structures: []
                        };
                        for (const structureItem of importPartStructure.structures) {
                            const structureItemPart = await partRepository.getByDecimalNumber(structureItem.decimal_num);
                            const structureItemParams = {
                                user_id: user.user_id,
                                parent_id: parentPart.part_id,
                                part_id: structureItemPart.part_id,
                                count: structureItem.count
                            };
                            importResult.structures.push(await partStructureRepository.add(structureItemParams))
                        }
                        const newMessage: IImportMessage<IPartStructures> = {
                            id,
                            item_type: "part_structures",
                            status: "added",
                            item: importResult,
                            intersection: null
                        };
                        messages.push(newMessage);
                    } catch (error) {
                        //Error branch
                        const newMessage: IImportMessage<IPartStructuresSimple> = {
                            id,
                            item_type: "part_structures",
                            status: "failure",
                            item: importPartStructure,
                            error_message: error.toString()
                        };
                        messages.push(newMessage);
                    }
                } else {
                    let dbResult = {
                        part_id: parentPart.part_id,
                        name: parentPart.name,
                        decimal_num: parentPart.decimal_num,
                        structures: dbPartStructure
                    };
                    let importResult = {
                        part_id: parentPart.part_id,
                        name: parentPart.name,
                        decimal_num: parentPart.decimal_num,
                        structures: []
                    };
                    for (const structureItem of importPartStructure.structures) {
                        const structureItemPart = await partRepository.getByDecimalNumber(structureItem.decimal_num);
                        structureItemPart.count = structureItem.count;
                        importResult.structures.push(structureItemPart)
                    }
                    if (!structureEqual(importResult, dbResult)) {
                        const newMessage: IImportMessage<IPartStructures> = {
                            id,
                            item_type: "part_structures",
                            status: "intersection",
                            item: importResult,
                            intersection: dbResult
                        };
                        messages.push(newMessage);
                    }
                }
                id = id + 1;
            }
        }
    }
    return messages
};


const structureEqual = (ip: IPartStructures, dbp: IPartStructures) => {
    let importPart = ip;
    let dbPart = dbp;

    if (importPart.structures.length !== dbPart.structures.length) {
        return false
    }
    for (const sp of importPart.structures) {
        const result = dbPart.structures.find(p => p.decimal_num == sp.decimal_num && p.count == sp.count);
        if (!result) {
            return false
        }
    }
    return true
};

const materialsEqual = (ip: IPartMaterials, dbp: IPartMaterials) => {
    let importPart = ip;
    let dbPart = dbp;

    if (importPart.materials.length !== dbPart.materials.length) {
        return false
    }
    for (const sp of importPart.materials) {
        const result = dbPart.materials.find(p => p.material_id == sp.material_id && p.count == sp.count && p.kd_count == sp.kd_count && p.comment == sp.comment);
        if (!result) {
            return false
        }
    }
    return true
};


export const ImportController = {
    importGroups: async (req, res) => {
        const result = getGroupsIntersection(req.body);
        SendResponse(res, result);
    },
    importUndergroups: async (req, res) => {
        SendResponse(res, getUndergroupsIntersection(req.body));
    },

    importMaterials: async (req, res) => {
        SendResponse(res, getMaterialsIntersection(req.body));
    },

    importParts: async (req, res) => {
        SendResponse(res, getPartsIntersection(req.body, req.user.recordset[0]));
    },

    importPartMaterials: async (req, res) => {
        SendResponse(res, getPartMaterialsIntersection(req.body, req.user.recordset[0]));
    },

    importPartStructures: async (req, res) => {
        SendResponse(res, getPartStructuresIntersection(req.body, req.user.recordset[0]));
    },
};