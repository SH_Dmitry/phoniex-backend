import express = require("express");
import {needAuth} from "../../../security/auth.service";
import {ImportController} from "./import.controller";


export const importRouter = express.Router({ mergeParams: true });

importRouter.post("/group", needAuth, ImportController.importGroups);
importRouter.post("/undergroup", needAuth, ImportController.importUndergroups);
importRouter.post("/material", needAuth, ImportController.importMaterials);
importRouter.post("/part", needAuth, ImportController.importParts);
importRouter.post("/part/materials", needAuth, ImportController.importPartMaterials);
importRouter.post("/part/structures", needAuth, ImportController.importPartStructures);