import {IRepository} from "../../database/sql.base.repository";

//Domain selectors
export type IMaterial = {
    material_id: string
    material: string
    forma?: string
    sortament?: string
    undergroup_id: string
}
export type IGroup = {
    group_id: string
    name: string
    number: number
}
export type IUndergroup = {
    undergroup_id: string
    name: string
    group_id: string
    number: number
}


//Repository interface
export interface IMaterialRepository extends IRepository{
    get(undergroup_id: string): Promise<Array<IMaterial>>
    getById(material_id: string): Promise<IMaterial>
    add(material: IMaterial): Promise<Array<IMaterial>>
    update(material: IMaterial): Promise<Array<IMaterial>>
    delete(material_id: string): void
}

export interface IUndergroupRepository extends IRepository{
    get(group_id: string): Promise<Array<IUndergroup>>
    add(undergroup: IUndergroup): void
    update(undergroup: IUndergroup): Promise<Array<IUndergroup>>
    delete(undergroup_id: string): void
}

export interface IGroupRepository extends IRepository{
    get(): Promise<Array<IGroup>>
    add(group: IGroup): void
    update(group: IGroup): Promise<Array<IGroup>>
    delete(group_id: string): void
}

