import {IUndergroup, IUndergroupRepository} from "../types";
import {SqlBaseRepository} from "../../../database/sql.base.repository";
import {getConnection} from "../../../database/sql.connector";

export class MaterialUndergroupRepository extends SqlBaseRepository implements IUndergroupRepository{
    async get(group_id:string) {
        const result = await (await getConnection())
            .input('group_id', group_id)
            .query('SELECT * FROM dbo.Undergroups WHERE group_id = @group_id');
        return  result.recordset;
    }
    async getById(undergroup_id:string) {
        const result = await (await getConnection())
            .input('undergroup_id', undergroup_id)
            .query('SELECT * FROM dbo.Undergroups WHERE undergroup_id = @undergroup_id');
        return  result.recordset[0];
    }
    async find(undergroup:IUndergroup) {
        const result = await (await getConnection())
            .input('name', undergroup.name)
            .input('group_id', undergroup.group_id)
            .query('SELECT * FROM dbo.Undergroups WHERE name = @name AND group_id = @group_id');
        return  result.recordset[0];
    }
    async getLastIndex() {
        const result = await (await getConnection())
            .query('SELECT MAX(undergroup_id) as id FROM dbo.Undergroup');
        return result.recordset[0];
    }
    async getAll() {
        const result = await (await getConnection())
            .query('SELECT * FROM dbo.Undergroups');
        return  result.recordset;
    }
    async add(undergroup: IUndergroup) {
        await (await getConnection())
            .query(`INSERT INTO dbo.Undergroups (undergroup_id, name, number, group_id) VALUES` +
                `('${undergroup.undergroup_id}','${undergroup.name}','${undergroup.number||"100"}', '${undergroup.group_id}')`);
    }
    async update(undergroup:IUndergroup) {
        try {
            const result = await (await getConnection())
                .input('undergroup_id', undergroup.undergroup_id)
                .input('name', undergroup.name)
                .input('number', undergroup.number)
                .input('group_id', undergroup.group_id)
                .query('UPDATE dbo.Undergroups SET name=@name, number=@number, group_id=@group_id WHERE undergroup_id = @undergroup_id; SELECT * FROM dbo.Undergroups WHERE undergroup_id=@undergroup_id');
            return result.recordset[0];
        }
        catch (e) {
            console.log(e)
        }
    }
    async delete(undergroup_id:string) {
        try {
            await (await getConnection())
                .input('undergroup_id', undergroup_id)
                .query('DELETE FROM dbo.Undergroups WHERE undergroup_id = @undergroup_id');
        }
        catch (e) {
            console.log(e)
        }
    }
}