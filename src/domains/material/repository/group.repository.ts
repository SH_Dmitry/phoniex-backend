import {IGroup, IGroupRepository} from "../types";
import {SqlBaseRepository} from "../../../database/sql.base.repository";
import {getConnection} from "../../../database/sql.connector";

export class MaterialGroupRepository extends SqlBaseRepository implements IGroupRepository {
    async get() {
        const result = await (await getConnection())
            .query(`SELECT dbo.Groups.group_id, dbo.Groups.name FROM dbo.Groups`);
        return result.recordset;
    }
    async getById(group_id: string) {
        const result = await (await getConnection())
            .query(`SELECT group_id, name, number FROM dbo.Groups WHERE group_id = ${group_id}`);
        return result.recordset[0];
    }
    async getLastIndex() {
        const result = await (await getConnection())
            .query('SELECT MAX(group_id) as id FROM dbo.Group');
        return result.recordset[0];
    }
    async add(group: IGroup) {
        try {
            await (await getConnection())
                .query(`INSERT INTO dbo.Groups (group_id, name, number) VALUES ('${group.group_id}','${group.name}','${group.number || 100}')`);
        } catch (e) {
            console.log(e)
        }
    }

    async delete(group_id: string) {
        try {
            await (await getConnection())
                .input('group_id', group_id)
                .query('DELETE FROM dbo.Groups WHERE group_id = @group_id');
        }
        catch (e) {
            console.log(e)
        }
    }

    async update(group: IGroup) {
        try {
        const result = await (await getConnection())
                .input('group_id', group.group_id)
                .input('name', group.name)
                .input('number', group.number)
                .query('UPDATE dbo.Groups SET name=@name, number=@number WHERE group_id = @group_id; SELECT * FROM dbo.Groups WHERE group_id=@group_id');
            return result.recordset[0];
        }
        catch (e) {
            console.log(e)
        }
    }
}