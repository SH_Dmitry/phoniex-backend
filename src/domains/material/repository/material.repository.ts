import {IMaterial, IMaterialRepository} from "../types";
import {SqlBaseRepository} from "../../../database/sql.base.repository";
import {getConnection} from "../../../database/sql.connector";

export class MaterialRepository extends  SqlBaseRepository implements IMaterialRepository {
    async get(undergroup_id: string) {
        const result = await (await getConnection())
            .input('undergroup_id', undergroup_id)
            .query('SELECT * FROM dbo.Materials WHERE undergroup_id = @undergroup_id');
        return result.recordset;
    }
    async getById(material_id: string) {
        const result = await (await getConnection())
            .input('material_id', material_id)
            .query('SELECT * FROM dbo.Materials WHERE material_id = @material_id');
        return result.recordset[0];
    }
    async getLastIndex() {
        const result = await (await getConnection())
            .query('SELECT MAX(material_id) as id FROM dbo.Materials');
        return result.recordset[0];
    }
    async find(material: IMaterial) {
        const result = await (await getConnection())
            .input('material', material.material)
            .input('sortament', material.sortament)
            .input('forma', material.forma)
            .query('SELECT * FROM dbo.Materials WHERE material = @material AND sortament = @sortament AND forma = @forma');
        return result.recordset[0];
    }
    async add(material: IMaterial) {
        const result = await (await getConnection())
            .input('material_id', material.material_id)
            .input('undergroup_id', material.undergroup_id)
            .input('sortament', material.sortament ? material.sortament : null)
            .input('material', material.material)
            .input('forma', material.forma ? material.forma : null)
            .execute('Materials_Add');
        return result.recordset[0];
    }
    async update(material: IMaterial) {
        const result = await (await getConnection())
            .input('material_id', material.material_id)
            .input('sortament', material.sortament ? material.sortament : null)
            .input('material', material.material ? material.material : null)
            .input('forma', material.forma ? material.forma : null)
            .execute('Materials_Update');
        return result.recordset[0];
    }
    async delete(material_id: string) {
        await (await getConnection())
            .input('material_id', material_id)
            .query('DELETE FROM dbo.Materials WHERE material_id = @material_id');
    }
}



