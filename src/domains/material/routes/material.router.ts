import express = require("express");
import {MaterialController} from "../controllers/material.controller";
import {needAuth} from "../../../security/auth.service";
import {MaterialGroupController} from "../controllers/group.controller";
import {MaterialUndergroupController} from "../controllers/undergroup.controller";

export const materialRouter = express.Router({ mergeParams: true });

materialRouter.get("/group", needAuth, MaterialGroupController.getGroups)
    .put("/group/:group_id", needAuth, MaterialGroupController.updateGroup)
    .delete("/group/:group_id", needAuth, MaterialGroupController.deleteGroup)
    .post("/group", needAuth, MaterialGroupController.addGroup)

    .get("/group/:group_id/undergroup", needAuth, MaterialUndergroupController.getUndergroups)
    .put("/undergroup/:undergroup_id", needAuth, MaterialUndergroupController.updateUndergroup)
    .delete("/undergroup/:undergroup_id", needAuth, MaterialUndergroupController.deleteUndergroup)
    .post("/undergroup", needAuth, MaterialUndergroupController.addUndergroup)

    .get("/:undergroup_id", needAuth, MaterialController.getMaterials)
    .put("/:material_id", needAuth, MaterialController.updateMaterial)
    .delete("/:material_id", needAuth, MaterialController.deleteMaterial)
    .post("", needAuth, MaterialController.addMaterial);
