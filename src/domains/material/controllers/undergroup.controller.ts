import {MaterialUndergroupRepository} from "../repository/undergroup.repository";
import {SendResponse} from "../../../middleware/response.handler";

const repository = new MaterialUndergroupRepository();

export const MaterialUndergroupController = {
    getUndergroups: async (req, res) => {
        const query = repository.get(req.params.group_id);
        SendResponse(res, query);
    },
    addUndergroup: async (req, res) => {
        const response = await repository.getLastIndex();
        const query = repository.add({
            undergroup_id: response.id + 1,
            name: req.body.name,
            number: req.body.number,
            group_id: req.body.group_id
        });
        SendResponse(res, query);
    },
    deleteUndergroup: async (req, res) => {
        const query = repository.delete(req.params.group_id);
        SendResponse(res, query, false);
    },
    updateUndergroup: async (req, res) => {
        const query = repository.update({
            undergroup_id: req.params.undergroup_id,
            name: req.body.name,
            number: req.body.number,
            group_id: req.body.group_id
        });
        SendResponse(res, query);
    }
};