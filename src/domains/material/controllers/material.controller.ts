import {MaterialRepository} from "../repository/material.repository";
import {SendResponse} from "../../../middleware/response.handler";

const repository = new MaterialRepository();

export const MaterialController = {
    getMaterials: async (req, res) => {
        const query = repository.get(req.params.undergroup_id);
        SendResponse(res, query);
    },
    addMaterial: async (req, res) => {
        const response = await repository.getLastIndex();
        const query = repository.add({
            material_id: response.id + 1,
            sortament: req.body.sortament,
            material: req.body.material,
            forma: req.body.forma,
            undergroup_id: req.body.undergroup_id
        });
        SendResponse(res, query);
    },
    deleteMaterial: async (req, res) => {
        const query = repository.delete(req.params.material_id);
        SendResponse(res, query, false);
    },
    updateMaterial: async (req, res) => {
        const query = repository.update({
            material_id: req.params.material_id,
            sortament: req.body.sortament,
            material: req.body.material,
            forma: req.body.forma,
            undergroup_id: req.body.undergroup_id
        });
        SendResponse(res, query);
    }
};