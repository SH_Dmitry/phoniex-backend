import {MaterialGroupRepository} from "../repository/group.repository";
import {SendResponse} from "../../../middleware/response.handler";

const repository = new MaterialGroupRepository();

export const MaterialGroupController = {
    getGroups: async (req, res) => {
        const query = repository.get();
        SendResponse(res, query);
    },
    addGroup: async (req, res) => {
        const response = await repository.getLastIndex();
        const query = repository.add({
            group_id: response.id + 1,
            name: req.body.name,
            number: req.body.number,
        });
        SendResponse(res, query);
    },
    deleteGroup: async (req, res) => {
        const query = repository.delete(req.params.group_id);
        SendResponse(res, query, false);
    },
    updateGroup: async (req, res) => {
        const query = repository.update({
            group_id: req.params.group_id,
            name: req.body.name,
            number: req.body.number,
        });
        SendResponse(res, query);
    }
};