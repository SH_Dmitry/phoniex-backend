import {DocReportBuilder} from "../../helpers/builders/doc.report.builder";
import {SendFile} from "../../middleware/response.handler";

export async function reportMaterial (req, res) {
        const reportBuilder = new DocReportBuilder(req.params.part_id);
        const report = await reportBuilder.createReport(req.body.name, req.body.decimal_num);
        SendFile(res, report, 'Full_material_report_' + req.params.part_id)
}
