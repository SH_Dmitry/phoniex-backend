import express = require("express");
import reportsController = require("./report.controller");

export const reportRouter = express.Router();

reportRouter.post("/material/:part_id", reportsController.reportMaterial);
