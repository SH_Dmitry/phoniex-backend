import {SqlBaseRepository} from "../../database/sql.base.repository";
import {
    IAddPartRequest,
    IDeletePartRequest,
    IOption,
    IPagination,
    IPart,
    IPartSimple,
    IPartsRepository,
    IUpdatePartRequest,
} from "./types";
import {getConnection} from "../../database/sql.connector";

export class PartsRepository extends SqlBaseRepository implements IPartsRepository {
    async get(options: IOption) {
        return {
            part_count: await this.getCountByType(options),
            parts: await this.getParts(options)
        };
    }

    async getCountByType(options: IOption): Promise<number> {
        let baseQuery: string = `SELECT COUNT(*) FROM Parts WHERE deleted=0`;
        for (let i = 0; i < options.filters.length; i++) {
            if (options.filters[i].exactly) {
                baseQuery = baseQuery + ` AND ${options.filters[i].column} = '${options.filters[i].value}'`
            } else {
                baseQuery = baseQuery + ` AND ${options.filters[i].column} LIKE '%${options.filters[i].value}%'`
            }
        }
        const result = await (await getConnection()).query(baseQuery);
        return result.recordsets[0][0][""]
    }

    async getParts(options: IOption): Promise<Array<IPart>> {
        let baseQuery: string = `SELECT ROW_NUMBER() OVER (ORDER BY ${options.sort.column} ${options.sort.direct}) ` +
            `AS RowNum, Parts.*, has_materials,has_structure,has_techprocess FROM Parts LEFT JOIN PartsMeta ON Parts.part_id = PartsMeta.part_id WHERE deleted=0`;
        for (let i = 0; i < options.filters.length; i++) {
            if (options.filters[i].exactly) {
                baseQuery = baseQuery + ` AND ${options.filters[i].column} = '${options.filters[i].value}'`
            } else {
                baseQuery = baseQuery + ` AND ${options.filters[i].column} LIKE '%${options.filters[i].value}%'`
            }
        }
        const query = this.WithPagination<IPartSimple>(baseQuery, options.pagination);
        const result = await (await getConnection()).query(query);
        return result.recordset;
    }


    WithPagination<T>(query: string, pagination: IPagination): string {
        return `WITH CTE AS ( ${query} ) SELECT * FROM CTE WHERE (RowNum > ${pagination.page_size * (pagination.page - 1)}) ` +
            `AND (RowNum <= ${pagination.page_size * (pagination.page)}) Order By RowNum `
    }

    async getById(part_id: string): Promise<IPartSimple> {
        const result = await (await getConnection())
            .input('part_id', part_id)
            .query('SELECT * FROM dbo.Parts WHERE part_id=@part_id');
        return result.recordset[0];
    }

    async getByDecimalNumber(decimal_num: string): Promise<IPartSimple> {
        const result = await (await getConnection())
            .input('decimal_num', decimal_num)
            .query('SELECT * FROM dbo.Parts WHERE decimal_num=@decimal_num');
        return result.recordset[0];
    }

    async getLastIndex() {
        const result = await (await getConnection())
            .query('SELECT MAX(part_id) as id FROM dbo.Parts');
        return result.recordset[0];
    }

    async add(req: IAddPartRequest): Promise<Array<IPart>> {
        const findPartResult = await (await getConnection())
            .input('decimal_num', req.part.decimal_num)
            .query('SELECT * FROM dbo.Parts WHERE decimal_num = @decimal_num;');
        const part = findPartResult.recordset[0];
        let addedPartResult;
        if (!part) {
            //Insert part
            await (await getConnection())
                .input('part_id', req.part.part_id)
                .input('name', req.part.name)
                .input('decimal_num', req.part.decimal_num)
                .input('type_id', req.part.type_id)
                .query('INSERT INTO dbo.Parts(part_id,name,decimal_num,type_id) VALUES (@part_id,@name,@decimal_num,@type_id)');
            console.log(req.part);
            //create meta
            await (await getConnection())
                .input('part_id', req.part.part_id)
                .query('INSERT INTO dbo.PartsMeta(part_id,description, image) VALUES (@part_id,null,null)');
            //create history
            await (await getConnection())
                .input('user_id', req.user_id)
                .input('part_id', req.part.part_id)
                .input('operation_id', "1")
                .execute('History_Add');
            //return part
            addedPartResult = await (await getConnection())
                .input('part_id', req.part.part_id)
                .query('SELECT dbo.Parts.*, has_materials,has_structure,has_techprocess FROM Parts LEFT JOIN PartsMeta ON Parts.part_id = PartsMeta.part_id WHERE Parts.part_id = @part_id;');
        } else {
            if (part.deleted > 0) {
                await (await getConnection())
                    .input('part_id', req.part.part_id)
                    .query('UPDATE dbo.Parts SET deleted = 0 WHERE part_id=@part_id');
                //create history
                await (await getConnection())
                    .input('user_id', req.user_id)
                    .input('part_id', req.part.part_id)
                    .input('operation_id', "4")
                    .execute('History_Add');
                addedPartResult = await (await getConnection())
                    .input('part_id', req.part.part_id)
                    .query('SELECT * FROM dbo.Parts WHERE part_id = @part_id');
            }
        }
        return addedPartResult.recordset[0];
    }

    async update(req: IUpdatePartRequest): Promise<Array<IPart>> {
        const result = await (await getConnection())
            .input('user_id', req.user_id)
            .input('part_id', req.part.part_id)
            .input('name', req.part.name)
            .input('decimal_num', req.part.decimal_num)
            .input('type_id', req.part.type_id)
            .execute('Parts_Update');
        return result.recordset[0];
    }

    async delete(req: IDeletePartRequest): Promise<void> {
        await (await getConnection())
            .input('user_id', req.user_id)
            .input('part_id', req.part_id)
            .execute('Parts_Delete');
    }


}