import express = require("express");
import {needAuth} from "../../../security/auth.service";
import {PartStructureController} from "./part.structure.controller";

export const partStructureRouter = express.Router({ mergeParams: true });

//Part part.structure ApiHelper
partStructureRouter.get("", needAuth, PartStructureController.get);
partStructureRouter.post("", needAuth, PartStructureController.add);
partStructureRouter.post("/replace", needAuth, PartStructureController.replace);
partStructureRouter.put("", needAuth, PartStructureController.update);
partStructureRouter.delete("", needAuth, PartStructureController.delete);