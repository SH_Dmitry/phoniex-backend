import {SqlBaseRepository} from "../../../database/sql.base.repository";
import {
    IAddStructurePartReq,
    IDeleteStructurePartReq,
    IPartStructureItem,
    IPartStructureRepository,
    IUpdateStructurePartReq
} from "./types";
import {getConnection} from "../../../database/sql.connector";

export class PartStructureRepository extends SqlBaseRepository implements IPartStructureRepository {
   async get(part_id: string): Promise<Array<IPartStructureItem>> {
        const result = await (await getConnection())
            .input('part_id', part_id)
            .execute('Parts_Structure_Get');
        return result.recordset;
    }

   async add(req: IAddStructurePartReq): Promise<IPartStructureItem> {
       const result = await (await getConnection())
           .input('user_id', req.user_id)
           .input('parent_id', req.parent_id)
           .input('part_id', req.part_id)
           .input('count', req.count? req.count : 1)
           .execute('Parts_Structure_Add');
       return  result.recordset[0];
    }
   async update(req: IUpdateStructurePartReq): Promise<IPartStructureItem> {
       const result = await (await getConnection())
           .input('user_id', req.user_id)
           .input('parent_id', req.parent_id)
           .input('part_id', req.part_id)
           .input('count', req.count)
           .execute('Parts_Structure_UpdatePartCount');
       return  result.recordset[0];
    }

   async delete(req: IDeleteStructurePartReq): Promise<void> {
       await (await getConnection())
           .input('user_id', req.user_id)
           .input('parent_id', req.parent_id)
           .input('part_id', req.part_id)
           .execute('Parts_Structure_Delete');

    }

}