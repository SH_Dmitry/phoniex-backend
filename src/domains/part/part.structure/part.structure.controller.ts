import {PartStructureRepository} from "./part.structure.repository";
import {SendResponse} from "../../../middleware/response.handler";
import {PartsRepository} from "../parts.repository";

const repository = new PartStructureRepository();
const repositoryPart = new PartsRepository();

export const PartStructureController = {
    get: async (req, res) => {
        const query = repository.get(req.params.part_id);
        SendResponse(res, query);
    },
    add: async (req, res) => {
        const query = repository.add({
            user_id: req.user.recordset[0].user_id,
            parent_id: req.params.part_id,
            part_id: req.body.part_id
        });
        SendResponse(res, query);
    },
    update: async (req, res) => {
        const query = repository.update({
            user_id: req.user.recordset[0].user_id,
            parent_id: req.params.part_id,
            part_id: req.body.part_id,
            count: req.body.count,
        });
        SendResponse(res, query);
    },

    replace: async (req, res) => {
        const replace = async () => {
            const parent = await repositoryPart.getByDecimalNumber(req.body.parent_decimal);
            console.log(req.body);
            const oldItems = await repository.get(parent.part_id);
            await Promise.all(oldItems.map((item) => {
                console.log("delete");
                repositoryPart.getByDecimalNumber(item.decimal_num).then(({part_id})=>{
                    return repository.delete({
                        user_id: req.user.recordset[0].user_id,
                        parent_id: parent.part_id,
                        part_id: part_id,
                    });
                })
            }));
            await Promise.all(req.body.items.map((item) => {
                repositoryPart.getByDecimalNumber(item.decimal_num).then(({part_id})=>{
                    console.log("add");
                return repository.add({
                    user_id: req.user.recordset[0].user_id,
                    parent_id: parent.part_id,
                    part_id: part_id,
                    count: item.count
                });
                })
            }))
        };
        const query = replace();
        SendResponse(res, query, false);
    },
    delete: async (req, res) => {
        const query = repository.delete({
            user_id: req.user.recordset[0].user_id,
            parent_id: req.params.part_id,
            part_id: req.body.part_id,
        });
        SendResponse(res, query, false);
    }
};
