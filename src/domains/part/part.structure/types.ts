//Domain type
import {IRepository} from "../../../database/sql.base.repository";

//Part structure item types
export type IPartStructureSimpleItem = {
    decimal_num: string,
    count: string
}
export type IPartStructureItem = {
    part_id:string,
    name:string,
    decimal_num: string,
    type_id: string
    count: string
}

//Part structure types
export type IPartStructuresSimple = {
    decimal_num: string,
    structures: Array<IPartStructureSimpleItem>
}
export type IPartStructures = {
    part_id: string,
    name: string
    decimal_num: string,
    structures: Array<IPartStructureItem>
}

//Req selectors
export type IAddStructurePartReq = {
    user_id: string
    parent_id:string
    part_id:string
    count?: string
}

export type IUpdateStructurePartReq = {
    user_id: string
    parent_id:string
    part_id: string
    count: number
}

export type IDeleteStructurePartReq = {
    user_id: string
    parent_id:string
    part_id: string

}

//Repository interface
export interface IPartStructureRepository extends IRepository{
    get(part_id:string): Promise<Array<IPartStructureItem>>
    add(req: IAddStructurePartReq): Promise<IPartStructureItem>
    update(req: IUpdateStructurePartReq): Promise<IPartStructureItem>
    delete(req: IDeleteStructurePartReq): Promise<void>
}