import {PartsController} from "./parts.controller";
import {needAuth} from "../../security/auth.service";

import {partMaterialsRouter} from "./part.materials/part.material.router";
import {partHistoryRouter} from "./part.history/part.history.roter";
import {partHierarchyRouter} from "./part.hierarchy/part.hierarchy.router";
import {partStructureRouter} from "./part.structure/part.structure.router";
import {commentsRouter} from "../comment/comments.router";
import express = require("express");

export const partsRouter = express.Router({ mergeParams: true });

//Parts ApiHelper
partsRouter.get("/:part_id", needAuth, PartsController.getById);
partsRouter.post("/list", PartsController.get);
partsRouter.post("", needAuth, PartsController.add);
partsRouter.delete("/:part_id", needAuth, PartsController.delete);
partsRouter.put("/:part_id", needAuth, PartsController.update);

//Part part.info ApiHelper
partsRouter.use("/:part_id/structure", partStructureRouter);
partsRouter.use("/:part_id/hierarchy", partHierarchyRouter);
partsRouter.use("/:part_id/comment", commentsRouter);
partsRouter.use("/:part_id/history", partHistoryRouter);
partsRouter.use("/:part_id/material", partMaterialsRouter);



