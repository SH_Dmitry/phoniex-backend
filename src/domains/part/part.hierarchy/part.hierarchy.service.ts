import {IHierarchyPart} from "./types";
import {PartHierarchyRepository} from "./part.hierarchy.repository";

const repository = new PartHierarchyRepository();

export async function getPartCountingList(part_id:string) {
    const partList = await repository.get(part_id);
    const flatPartList = startRecursePartCountingList(part_id, partList);
    flatPartList.push({part_id, count: 1});
    return  flatPartList
}


//RUS// Получение плоского массива ДСЕ с учетом иерархического вхождения
function startRecursePartCountingList(part_id: string, data: Array<IHierarchyPart>){
    let result = [] as Array<{
        part_id: string,
        count: number }>;
    for (let item of data) {
        if (item.parent_id == part_id) {
            result.push({part_id: item.part_id, count: item.count});
            const childes = recursePartCountingList(item, data);
            result = result.concat(childes)
        }
    }
    return result
}

//RUS// Рекурсивная функция
function recursePartCountingList(parent: IHierarchyPart, data: Array<IHierarchyPart>) {
    let result = [] as Array<{
        part_id: string,
        count: number }>;
    for (let item of data) {
        if (item.parent_id == parent.part_id) {
            item.count = item.count * parent.count;
            result.push({part_id: item.part_id, count: item.count});
            const childes = recursePartCountingList(item, data);
            if (childes.length > 0) {
                result = result.concat(childes)
            }
        }
    }
    return result
}