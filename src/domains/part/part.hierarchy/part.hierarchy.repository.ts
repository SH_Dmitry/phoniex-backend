import {SqlBaseRepository} from "../../../database/sql.base.repository";
import {IHierarchyPart, IHierarchyPartsRepository} from "./types";
import {getConnection} from "../../../database/sql.connector";

export class PartHierarchyRepository extends SqlBaseRepository implements IHierarchyPartsRepository {
    async get(part_id: string): Promise<Array<IHierarchyPart>> {
        const result = await (await getConnection())
            .input('part_id', part_id)
            .execute('Parts_Hierarchy_Get');
        return result.recordset;
    }
}