import {IRepository} from "../../../database/sql.base.repository";

//Domain type
export interface IHierarchyPart {
    parent_id:string
    part_id: string
    name: string
    decimal_num: string
    type_id: string
    count?: number
    children?: Array<IHierarchyPart>
}

//Repository interface
export interface IHierarchyPartsRepository extends IRepository{
    get(part_id:string): Promise<Array<IHierarchyPart>>
}