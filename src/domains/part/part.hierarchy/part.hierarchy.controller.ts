import {PartHierarchyRepository} from "./part.hierarchy.repository";
import {IHierarchyPart} from "./types";
import {SendResponse} from "../../../middleware/response.handler";

const repository = new PartHierarchyRepository();

export const PartHierarchyController = {
    get: async (req, res) => {
        const result = await repository.get(req.params.part_id);
        const query = populatePartTree(req.params.part_id, result);
        SendResponse(res, query);
    }
};


type TreeItem = {
    key: string,
    part_id: string,
    type_id: string
    title: string,
    count: number,
    children: Array<TreeItem>
}

const populatePartTree = async (part_id: string, parts: Array<IHierarchyPart>) => {
    let result = [];
    for (let i of parts) {

        if (i.parent_id.toString() == part_id) {
            let newPart: TreeItem = {
                key: part_id.toString() + i.part_id.toString() + Math.random(),
                part_id: i.part_id,
                title: i.name + " " + i.decimal_num,
                type_id: i.type_id,
                count: i.count,
                children: recursePopulatePartTree(i.part_id, parts),

            };
            result.push(newPart);
        }
    }
    return result
};

const recursePopulatePartTree = (parent_id: string, parts: Array<IHierarchyPart>) => {
    let result = [];
    for (let i of parts) {
        if (i.parent_id.toString() == parent_id) {
            let newPart: TreeItem = {
                key: parent_id.toString() + i.part_id.toString() + Math.random(),
                part_id: i.part_id,
                title: i.name + " " + i.decimal_num,
                type_id: i.type_id,
                count: i.count,
                children: recursePopulatePartTree(i.part_id, parts),
            };
            result.push(newPart);
        }
    }
    return result
};
