import express = require("express");
import {needAuth} from "../../../security/auth.service";
import {PartHierarchyController} from "./part.hierarchy.controller";

export const partHierarchyRouter = express.Router({ mergeParams: true });

//Part part.history ApiHelper
partHierarchyRouter.get("", needAuth, PartHierarchyController.get);