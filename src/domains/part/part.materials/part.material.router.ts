import express = require("express");
import {PartMaterialController} from "./part.materials.controller";
import {needAuth} from "../../../security/auth.service";

export const partMaterialsRouter = express.Router({ mergeParams: true });

//Part material ApiHelper

partMaterialsRouter.get("", needAuth, PartMaterialController.get);
partMaterialsRouter.post("", needAuth, PartMaterialController.add);
partMaterialsRouter.put("", needAuth, PartMaterialController.update);
partMaterialsRouter.delete("", needAuth, PartMaterialController.delete);