import {SqlBaseRepository} from "../../../database/sql.base.repository";
import {
    IAddParamPartMaterialReq,
    IAddPartMaterialReq,
    IDeletePartMaterialReq,
    IPartMaterialItem,
    IPartMaterialsRepository,
    IUpdatePartMaterialReq
} from "./types";
import {getConnection} from "../../../database/sql.connector";

export class PartMaterialsRepository extends  SqlBaseRepository implements IPartMaterialsRepository {
    async get(part_id: string): Promise<Array<IPartMaterialItem>> {
        const result = await (await getConnection())
            .input('part_id', part_id)
            .execute('Materials_Part_Get');
        return result.recordset;
    }
    async add(req: IAddPartMaterialReq): Promise<Array<IPartMaterialItem>> {
        const result = await (await getConnection())
            .input('user_id', req.user_id)
            .input('part_id', req.part_id)
            .input('material_id', req.material_id)
            .execute('Materials_Part_Add');
        return result.recordset;
    }

    async addWithParams(req: IAddParamPartMaterialReq): Promise<Array<IPartMaterialItem>> {
        const result = await (await getConnection())
            .input('user_id', req.user_id)
            .input('part_id', req.part_id)
            .input('material_id', req.material_id)
            .input('count', req.count)
            .input('kd_count', req.kd_count)
            .input('sizes', req.sizes)
            .input('comment', req.comment)
            .execute('Materials_Part_Add_Params');
        return result.recordset[0];
    }

   async update(req: IUpdatePartMaterialReq): Promise<Array<IPartMaterialItem>> {
        const result = await (await getConnection())
            .input('user_id', req.user_id)
            .input('part_id', req.part_id)
            .input('material_id', req.material_id)
            .input('count', req.count)
            .input('unit_id', req.unit_id)
            .input('kd_count', req.kd_count)
            .input('sizes', req.sizes)
            .input('comment', req.comment)
            .execute('Materials_Part_Update');
        return  result.recordset;
    }

   async delete(req: IDeletePartMaterialReq): Promise<void> {
        await (await getConnection())
            .input('user_id', req.user_id)
            .input('part_id', req.part_id)
            .input('material_id', req.material_id)
            .execute('Materials_Part_Delete');
    }
}



