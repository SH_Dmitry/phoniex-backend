import {PartMaterialsRepository} from "./part.materials.repository";
import {IPartMaterialItem} from "./types";

const repository = new PartMaterialsRepository();

//Get part.materials by part id
//RUS// Получение всех материалов испольуемыех в детале
export async function getMaterialsByPartId(part_id: string) {
    return await repository.get(part_id);
}

//Get part.materials by parts list
//RUS// Получение всех материалов испольуемыех в деталях из списка
export async function getMaterialsByPartList(parts: Array<{ part_id: string, count: number }>) {
    let result = [] as Array<IPartMaterialItem>;
    for (const item of parts) {
        const materials = await repository.get(item.part_id);
        for (let material of materials) {
            material.count = material.count * item.count
        }
        result = result.concat(materials)
    }
    return result
}

//RUS//Упорядочить материалы в списке (просуммировать количество)
export async function orderingMaterialList(materials: Array<IPartMaterialItem>) {
    const result = [] as Array<IPartMaterialItem>;
    for (let material of materials) {
        if (result.length > 0) {
            let isAdded = false;
            for (let item of result) {
                if (!isAdded) {
                    if (item.material_id === material.material_id&&item.comment==material.comment) {
                        item.count = item.count + material.count;
                        isAdded = true
                    }
                } else {
                    break
                }
            }
            if (!isAdded) {
                result.push(material);
                isAdded = true
            }
        } else {
            result.push(material)
        }
    }
    return result
}