import {PartMaterialsRepository} from "./part.materials.repository";
import {SendResponse} from "../../../middleware/response.handler";

const repository = new PartMaterialsRepository();

export const PartMaterialController = {
    get: async (req, res) => {
        const query = repository.get(req.params.part_id);
        SendResponse(res, query);
    },
    add: async (req, res) => {
        const query = repository.add({
            user_id: req.user.recordset[0].user_id,
            part_id: req.params.part_id,
            material_id: req.body.material_id
        });
        SendResponse(res, query);
    },
    update: async (req, res) => {
        const query = repository.update({
            user_id: req.user.recordset[0].user_id,
            part_id: req.params.part_id,
            material_id: req.body.material_id,
            kd_count: req.body.kd_count,
            sizes: req.body.sizes,
            count: req.body.count,
            unit_id: req.body.unit_id,
            comment: req.body.comment
        });
        SendResponse(res, query);
    },
    delete: async (req, res) => {
        const query = repository.delete({
            user_id: req.user.recordset[0].user_id,
            part_id: req.params.part_id,
            material_id: req.body.material_id
        });
        SendResponse(res, query, false);
    }
};

