import {IRepository} from "../../../database/sql.base.repository";

//Domain type

//Part materials item types
export type IPartMaterialItem = {
    material_id: string,
    sortament?: string,
    material: string,
    forma?: string,
    count: number
    kd_count: string,
    sizes?: string,
    comment?: string,
    unit_id: string
    undergroup_id:string
}
export type IPartMaterialSimpleItem = {
    material_id: string,
    count: number
    kd_count: string,
    sizes?: string,
    comment?: string,
    unit_id: string
}

//Part materials types
export type IPartMaterialsSimple = {
    decimal_num: string,
    materials: Array<IPartMaterialSimpleItem>
}
export type IPartMaterials= {
    part_id: string,
    name: string,
    decimal_num: string,
    materials: Array<IPartMaterialItem>
}





//Req selectors
export type IAddPartMaterialReq = {
    user_id: string
    material_id: string,
    part_id: string,
}

export type IAddParamPartMaterialReq = {
    user_id: string
    part_id: string,
    material_id: string,
    count: number
    kd_count: string,
    sizes?: string,
    comment?: string,
    unit_id: string
}

export type IDeletePartMaterialReq = {
    user_id: string
    material_id: string,
    part_id: string,
}
export type IUpdatePartMaterialReq = {
    user_id: string
    material_id: string,
    part_id: string,
    count: number,
    kd_count: string,
    sizes?: string,
    comment?: string,
    unit_id: string
}

//Repository interface
export interface IPartMaterialsRepository extends IRepository{
    get(part_id: string): Promise<Array<IPartMaterialItem>>
    add(req: IAddPartMaterialReq): Promise<Array<IPartMaterialSimpleItem>>
    update(req: IUpdatePartMaterialReq): Promise<Array<IPartMaterialSimpleItem>>
    delete(req: IDeletePartMaterialReq): Promise<void>
}
