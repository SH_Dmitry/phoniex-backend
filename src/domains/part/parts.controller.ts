import {PartsRepository} from "./parts.repository";
import {SendResponse} from "../../middleware/response.handler";

const repository = new PartsRepository();

export const PartsController = {
    get: async (req, res) => {
        const query = repository.get(req.body);
        SendResponse(res, query);
    },
    getById: async (req, res) => {
        const query = repository.getById(req.params.part_id);
        SendResponse(res, query);
    },

    add: async (req, res) => {
        const response = await repository.getLastIndex();
        const query = repository.add({
            user_id: req.user.recordset[0].user_id,
            part: {
                part_id: response.id+1,
                name: req.body.name,
                decimal_num: req.body.decimal_num,
                type_id: req.body.type_id,
            }
        });
        SendResponse(res, query);
    },
    update: async (req, res) => {
        const query = repository.update({
            user_id: req.user.recordset[0].user_id,
            part: {
                part_id: req.params.part_id,
                name: req.body.name,
                decimal_num: req.body.decimal_num,
                type_id: req.body.type_id,
            }
        });
        SendResponse(res, query);
    },
    delete: async (req, res) => {
        const query = repository.delete({
            user_id: req.user.recordset[0].user_id,
            part_id: req.params.part_id
        });
        SendResponse(res, query, false);
    }
};



