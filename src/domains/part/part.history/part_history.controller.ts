import {PartHistoryRepository} from "./part.history.repository";
import {SendResponse} from "../../../middleware/response.handler";

const repository = new PartHistoryRepository();

export const PartHistoryController = {
    getPartHistory: async (req, res) => {
        const query = repository.get(req.params.part_id);
        SendResponse(res, query);
    }
};
