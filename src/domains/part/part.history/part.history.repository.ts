import {SqlBaseRepository} from "../../../database/sql.base.repository";
import {IPartHistory, IPartHistoryRepository} from "./types";
import {getConnection} from "../../../database/sql.connector";

export class PartHistoryRepository extends SqlBaseRepository implements IPartHistoryRepository {
    async get(part_id: string): Promise<Array<IPartHistory>> {
        const result = await (await getConnection())
            .input('part_id', part_id)
            .execute('History_Get');
        return result.recordset;
    }
}