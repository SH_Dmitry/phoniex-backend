import {IRepository} from "../../../database/sql.base.repository";

//Domain type
export interface IPartHistory {
    history_id: string
    user_id:string
    operation: string
    date_time: string
}

//Repository interface
export interface IPartHistoryRepository extends IRepository{
    get(part_id:string): Promise<Array<IPartHistory>>
}