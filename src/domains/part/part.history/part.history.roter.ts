import express = require("express");
import {needAuth} from "../../../security/auth.service";
import {PartHistoryController} from "./part_history.controller";

export const partHistoryRouter = express.Router({ mergeParams: true });

//Part part.history ApiHelper
partHistoryRouter.get("", needAuth, PartHistoryController.getPartHistory);