import {IRepository} from "../../database/sql.base.repository";

//Domain type
export interface IPart {
    part_id: string
    name: string
    decimal_num: string
    type_id: string
    deleted:number
    description?:string
    has_materials?: number
    has_structure?: number
    has_techprocess?: number
}

export interface IPartSimple {
    part_id: string
    name: string
    decimal_num: string
    type_id: string
}

//Req selectors
export type IAddPartRequest = {
    user_id: string,
    part: IPartSimple
}
export type IUpdatePartRequest = {
    user_id: string,
    part: IPartSimple
}

export type IDeletePartRequest = {
    user_id: string,
    part_id: string
}

//Repository interface
export interface IPartsRepository extends IRepository {
    get(req: IOption): Promise<IGetPartResponse>

    add(req: IAddPartRequest): Promise<Array<IPart>>

    update(req: IUpdatePartRequest): Promise<Array<IPart>>

    delete(req: IDeletePartRequest): Promise<void>
}

// Get part request body types
export  type IOption = {
    pagination: IPagination
    sort: ISort
    filters: Array<IFilter>
}

export type IPagination = {
    page: number
    page_size: number
}

export type ISort = {
    column: string
    direct: string
}

export type IFilter = {
    column: string
    value: string
    exactly: boolean
}


//Get part response type
export type IGetPartResponse = {
    part_count: number,
    parts: Array<IPart>
}