import {UserRepository} from "./user.repository";
import {SendResponse} from "../../middleware/response.handler";

const repository = new UserRepository();

export const UserController = {
    login: async (req, res) => {
        const query = repository.login({
            username: req.body.username,
            password: req.body.password
        });
        SendResponse(res, query);
    }
};
