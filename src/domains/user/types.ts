import {IRepository} from "../../database/sql.base.repository";

//Domain type
export type IAuthUser = {
    user_id: string
    personal_data: string
    state: string
    token:string
    image?: any
}
//Req selectors
export type ILoginReq = {
    username: string,
    password: string
}

//Repository interface
export interface IUserRepository extends IRepository{
    login(req: ILoginReq): Promise<IAuthUser>
}