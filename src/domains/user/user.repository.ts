import {SqlBaseRepository} from "../../database/sql.base.repository";
import {IAuthUser, ILoginReq, IUserRepository} from "./types";
import {getConnection} from "../../database/sql.connector";

export class UserRepository extends SqlBaseRepository implements IUserRepository {
    async login(req: ILoginReq): Promise<IAuthUser> {
        const result = await (await getConnection()).query
            `SELECT TOP 1 username, personal_data, state, token, image FROM dbo.Users WHERE username = ${req.username} AND password = ${req.password}`;
        return  result.recordset[0];
    }
}