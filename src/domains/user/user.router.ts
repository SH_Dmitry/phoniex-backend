import express = require("express");
import {UserController} from "./user.controllers";

export const userRouter = express.Router();

userRouter.post("/signin", UserController.login);
