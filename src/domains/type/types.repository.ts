import {SqlBaseRepository} from "../../database/sql.base.repository";
import {IType, ITypeRepository} from "./types";
import {getConnection} from "../../database/sql.connector";

export class TypeRepository extends SqlBaseRepository implements ITypeRepository {
    async getHistoryTypes(): Promise<Array<IType>> {
        return undefined
    }

    async getOperationTypes(): Promise<Array<IType>> {
        return undefined;
    }

    async getPartTypes(): Promise<Array<IType>> {
        const result = await (await getConnection())
            .query`SELECT * FROM dbo.PartTypes`;
        return result.recordset;
    }

}