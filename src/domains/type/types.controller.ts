import {SendResponse} from "../../middleware/response.handler";
import {TypeRepository} from "./types.repository";

const repository = new TypeRepository();

export const TypeController = {
    getPartTypes: async (req, res) => {
        const query = repository.getPartTypes();
        SendResponse(res, query);
    },
    getOperationTypes: async (req, res) => {
        const query = repository.getOperationTypes();
        SendResponse(res, query);
    },
    getHistoryTypes: async (req, res) => {
        const query = repository.getHistoryTypes();
        SendResponse(res, query);
    }
};