import express = require("express");
import {TypeController} from "./types.controller";

export const typeRouter = express.Router();

typeRouter.get("/part", TypeController.getPartTypes)
    .get("/operations", TypeController.getOperationTypes)
    .get("/history", TypeController.getHistoryTypes);
