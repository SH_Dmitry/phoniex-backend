import {IRepository} from "../../database/sql.base.repository";

//Domain type
export type IType = {
    id:string,
    name: string
}

//Repository interface
export interface ITypeRepository extends IRepository {
    getPartTypes(): Promise<Array<IType>>
    getOperationTypes(): Promise<Array<IType>>
    getHistoryTypes(): Promise<Array<IType>>
}