import {CommentsRepository} from "./comments.repository";
import {ICommentRes} from "./types";
import {SendResponse} from "../../middleware/response.handler";

const repository = new CommentsRepository();

export const CommentController = {

    addComment: async (req, res) => {
            const query = repository.addComment({
                part_id: req.params.part_id,
                user_id: req.user.recordset[0].user_id,
                text: req.body.text
            });
            SendResponse(res, query);
    },

    addReply: async (req, res) => {
            const query =  repository.addReply({
                comment_id: req.params.comment_id,
                user_id: req.user.recordset[0].user_id,
                text: req.body.text
            });
            SendResponse(res, query);
    },

    get: async (req, res) => {
            const response = await repository.get(req.params.part_id);
            const query = populateCommentTree(req.params.part_id, response);
            SendResponse(res, query);
    },
};


//Recurse comment tree builder
const populateCommentTree = async (part_id:string, comments:Array<ICommentRes>) => {
    let result = [];
    for (let i of comments) {
        if (i.parent_id.toString() === part_id) {
            i.replies = recursePopulateCommentTree(i.comment_id, comments);
            result.push(i);
        }
    }
    return result
};

const recursePopulateCommentTree = (comment_id:string, comments:Array<ICommentRes>) => {
    let result = [];
    for (let i of comments) {
        if (i.parent_id.toString() === comment_id) {
            i.replies = recursePopulateCommentTree(i.comment_id, comments);
            result.push(i);
        }
    }
    return result
};