import express = require("express");
import {CommentController} from "./comment.controller";
import {needAuth} from "../../security/auth.service";

export const commentsRouter = express.Router({mergeParams:true});

commentsRouter.get("", needAuth, CommentController.get)
    .post("", needAuth, CommentController.addComment)
    .post("/:comment_id/reply", needAuth, CommentController.addReply);
