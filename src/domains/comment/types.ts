import {IRepository} from "../../database/sql.base.repository";

//Domain type
export interface ICommentRes {
    user_id: string
    personal_data:string
    state: string
    image: any

    parent_id: string
    comment_id: string
    text: string
    date_time: string

    replies?: Array<ICommentRes>
}

//Req selectors
export type IAddCommentReq = {
    user_id: string
    part_id: string
    text: string
}
export type IAddReplyReq = {
    user_id: string,
    comment_id: string,
    text: string
}


//Repository interface
export interface ICommentsRepository extends IRepository {
    get(part_id:string): Promise<Array<ICommentRes>>

    addComment(req: IAddCommentReq): Promise<Array<ICommentRes>>

    addReply(req: IAddReplyReq): Promise<Array<ICommentRes>>
}
