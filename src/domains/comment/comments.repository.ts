import {IAddCommentReq, IAddReplyReq, ICommentRes, ICommentsRepository} from "./types";
import {SqlBaseRepository} from "../../database/sql.base.repository";
import {getConnection} from "../../database/sql.connector";

export class CommentsRepository extends SqlBaseRepository implements ICommentsRepository {
   async addComment(req: IAddCommentReq): Promise<Array<ICommentRes>> {
        const result = await (await getConnection())
            .input('part_id', req.part_id)
            .input('user_id', req.user_id)
            .input('text', req.text)
            .execute('Comments_Part_Add');
        return result.recordset;
    }

   async addReply(req: IAddReplyReq): Promise<Array<ICommentRes>> {
       const result = await (await getConnection())
           .input('comment_id', req.comment_id)
           .input('user_id', req.user_id)
           .input('text', req.text)
           .execute('Comments_Comment_Add');
       return result.recordset;
    }

   async get(part_id: string): Promise<Array<ICommentRes>> {
       const result = await (await getConnection())
           .input('part_id', part_id)
           .execute('Comments_Part_Get');
       return result.recordset
    }

}