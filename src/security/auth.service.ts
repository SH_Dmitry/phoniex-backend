import {bearerStrategy} from "./strategy/bearer.strategy";
import passport = require('passport');

export const setStrategy = () => passport.use('bearer', bearerStrategy());

export const needAuth = function (req, res, next) {
    let auth = passport.authenticate('bearer', {session: false});
    auth(req, res, function (err) {
        next(err);
    });
};