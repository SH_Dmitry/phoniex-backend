import {getConnection} from "../../database/sql.connector";

const BearerStrategy = require('passport-http-bearer').Strategy;

export const bearerStrategy = function () {
    return new BearerStrategy(
        function (token, callback) {
            getConnection().then(connection => {
                connection.query`SELECT * FROM users WHERE token = ${token}`
                    .then(function (user) {
                        return callback(null, user);
                    });
            });
        }
    );
};